/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.onee.dfc.ifpm.model;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

/**
 *
 * @author youne
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@Builder(toBuilder = true)
@AllArgsConstructor
public class Document {

    @Id
    @GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY )
    private Integer id ;
    private Date date;

    @ManyToOne
    private Animateur animateur;

    @ManyToOne
    private TypeDocument typeDocument;
}
