package org.onee.dfc.ifpm.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Builder(toBuilder = true)
@AllArgsConstructor
public class AnimTheme {
    @Id
    @GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY )
    private Integer id ;

    @ManyToOne
    private Animateur animateur;

    @ManyToOne
    private Theme theme;

    @OneToOne
    private Session session;

    private boolean sc;
    private boolean sa;
}
