/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.onee.dfc.ifpm.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author youne
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@Builder(toBuilder = true)
@AllArgsConstructor
public class Animateur implements Serializable {

    @Id
    @GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY )
    private Integer id;
    private String matricule;
    private String nomPrenom;
    private String email;
    private String tele;
    private Date dateDeNaissance;
    private Date dateEmbauche;
    private Date dateDepart;
    private Date age;
    private String sex;
    private String photo;
    private String cadreIntegration; // Fiha : FA / NR / Habilitation /Projet..etc i9dou ikounou bzaf
    private Date dateInscription;
    private String activeRetraite;
    private String responsable;
    private String formationPedagogique;
    private Date dateFP;
    private Date dateInvitationFP;
    private String Simulation;
    private Date dateSimulation;
    private String DecisionSimulation;// Fiha : autonome / a accompagner / refaire FP / incapable d'animer
    private String ficheAppreciaionEntretien;
    private String assistanceFoad; // oui/non
    private String capaciteAccompagnement;
    private String Accompagnateur; // nom de l'accompagnateur
    private String Observation1;
    private String Observation2;
    private String Observation3;

    @ManyToOne
    private TypeAnimateur typeAnimateur;

    @ManyToOne
    private Fonction fonction;

    @ManyToOne
    private Experience experience;

    @ManyToOne
    private Approuvation approuvation;

    @ManyToOne
    private Direction direction;

    @ManyToOne
    private DomainePerso domainePerso;

    @ManyToOne
    private SousDomain sousDomain;

}
