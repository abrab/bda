/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.onee.dfc.ifpm.model;

import lombok.*;

import javax.persistence.*;
import java.util.List;

/**
 *
 * @author youne
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@Builder(toBuilder = true)
@AllArgsConstructor
public class Theme {

    @Id
    @GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY )
    private Integer codetheme ; // TODO rename to id
    // TODO add a unique string codetheme
    private String intituletheme;
    private String dureetheme ;

    @ManyToOne
    private CategorieTheme categorie;

    @ManyToOne
    private PrioriteTheme priorite;

    @ManyToMany
    private List<TypeFormation> typeFormations;
    
}
