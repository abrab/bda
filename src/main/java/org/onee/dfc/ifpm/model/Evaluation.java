/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.onee.dfc.ifpm.model;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 *
 * @author youne
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@Builder(toBuilder = true)
@AllArgsConstructor
public class Evaluation {

    @Id
    @GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY )
    private Integer id ; 
    private BigDecimal methodolgie; 
    private BigDecimal animation; 
    private BigDecimal applicationAquis;
    private BigDecimal aspectsTechnique;
    private BigDecimal evaluationTheme;
    private BigDecimal evaluationTotal;

    @OneToOne
    private Session session;
}
