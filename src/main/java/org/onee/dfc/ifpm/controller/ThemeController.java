package org.onee.dfc.ifpm.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.IteratorUtils;
import org.onee.dfc.ifpm.config.Messages;
import org.onee.dfc.ifpm.model.Theme;
import org.onee.dfc.ifpm.repository.ThemeRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.annotation.SessionScope;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import org.onee.dfc.ifpm.controller.util.JsfUtil.PersistAction;
import org.onee.dfc.ifpm.controller.util.JsfUtil;

@Controller("themeController")
@SessionScope
@Slf4j
@RequiredArgsConstructor
public class ThemeController implements Serializable {

    private final ThemeRepository ejbFacade;
    private final Messages messages;

    private List<Theme> items = null;
    private Theme selected;

    public Theme getSelected() {
        return selected;
    }

    public void setSelected(Theme selected) {
        this.selected = selected;
    }

    private ThemeRepository getFacade() {
        return ejbFacade;
    }

    public Theme prepareCreate() {
        selected = new Theme();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, messages.get("ThemeCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, messages.get("ThemeUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, messages.get("ThemeDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Theme> getItems() {
        if (items == null) {
            items = IteratorUtils.toList(getFacade().findAll().iterator());
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().save(selected);
                } else {
                    getFacade().delete(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (Exception ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, messages.get("PersistenceErrorOccured"));
                }
            }
//            catch (Exception ex) {
//                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
//                JsfUtil.addErrorMessage(ex, messages.get("PersistenceErrorOccured"));
//            }
        }
    }

    public Theme getTheme(java.lang.Integer id) {
        return getFacade().findById(id).orElse(null);
    }

    public List<Theme> getItemsAvailableSelectMany() {
        return IteratorUtils.toList(getFacade().findAll().iterator());
    }

    public List<Theme> getItemsAvailableSelectOne() {
        return IteratorUtils.toList(getFacade().findAll().iterator());
    }

    @FacesConverter(forClass = Theme.class)
    public static class ThemeControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            ThemeController controller = (ThemeController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "themeController");
            return controller.getTheme(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Theme) {
                Theme o = (Theme) object;
                return getStringKey(o.getCodetheme());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Theme.class.getName()});
                return null;
            }
        }

    }

}
