package org.onee.dfc.ifpm.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.IteratorUtils;
import org.onee.dfc.ifpm.config.Messages;
import org.onee.dfc.ifpm.model.Proposition;
import org.onee.dfc.ifpm.repository.PropositionRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.annotation.SessionScope;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.onee.dfc.ifpm.controller.util.JsfUtil.PersistAction;
import org.onee.dfc.ifpm.controller.util.JsfUtil;

@Controller("propositionController")
@SessionScope
@Slf4j
@RequiredArgsConstructor
public class PropositionController implements Serializable {

    private final PropositionRepository ejbFacade;
    private final Messages messages;

    private List<Proposition> items = null;
    private Proposition selected;

    public Proposition getSelected() {
        return selected;
    }

    public void setSelected(Proposition selected) {
        this.selected = selected;
    }

    private PropositionRepository getFacade() {
        return ejbFacade;
    }

    public Proposition prepareCreate() {
        selected = new Proposition();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, messages.get("PropositionCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, messages.get("PropositionUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, messages.get("PropositionDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Proposition> getItems() {
        if (items == null) {
            items = IteratorUtils.toList(getFacade().findAll().iterator());
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().save(selected);
                } else {
                    getFacade().delete(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (Exception ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, messages.get("PersistenceErrorOccured"));
                }
            }
//            catch (Exception ex) {
//                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
//                JsfUtil.addErrorMessage(ex, messages.get("PersistenceErrorOccured"));
//            }
        }
    }

    public Proposition getProposition(java.lang.Integer id) {
        return getFacade().findById(id).orElse(null);
    }

    public List<Proposition> getItemsAvailableSelectMany() {
        return IteratorUtils.toList(getFacade().findAll().iterator());
    }

    public List<Proposition> getItemsAvailableSelectOne() {
        return IteratorUtils.toList(getFacade().findAll().iterator());
    }

    @FacesConverter(forClass = Proposition.class)
    public static class PropositionControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            PropositionController controller = (PropositionController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "propositionController");
            return controller.getProposition(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Proposition) {
                Proposition o = (Proposition) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Proposition.class.getName()});
                return null;
            }
        }

    }

}
