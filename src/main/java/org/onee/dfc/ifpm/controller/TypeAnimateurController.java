package org.onee.dfc.ifpm.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.IteratorUtils;
import org.onee.dfc.ifpm.config.Messages;
import org.onee.dfc.ifpm.controller.util.JsfUtil;
import org.onee.dfc.ifpm.controller.util.JsfUtil.PersistAction;
import org.onee.dfc.ifpm.model.TypeAnimateur;
import org.onee.dfc.ifpm.repository.TypeAnimateurRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.annotation.SessionScope;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

@Controller("typeAnimateurController")
@SessionScope
@Slf4j
@RequiredArgsConstructor
public class TypeAnimateurController implements Serializable {

    private final TypeAnimateurRepository ejbFacade;
    private final Messages messages;

    private List<TypeAnimateur> items = null;
    private TypeAnimateur selected;

    public TypeAnimateur getSelected() {
        return selected;
    }

    public void setSelected(TypeAnimateur selected) {
        this.selected = selected;
    }

    private TypeAnimateurRepository getFacade() {
        return ejbFacade;
    }

    public TypeAnimateur prepareCreate() {
        selected = new TypeAnimateur();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, messages.get("TypeAnimateurCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, messages.get("TypeAnimateurUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, messages.get("TypeAnimateurDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<TypeAnimateur> getItems() {
        if (items == null) {
            items = IteratorUtils.toList(getFacade().findAll().iterator());
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().save(selected);
                } else {
                    getFacade().delete(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (Exception ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, messages.get("PersistenceErrorOccured"));
                }
            }
//            catch (Exception ex) {
//                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
//                JsfUtil.addErrorMessage(ex, messages.get("PersistenceErrorOccured"));
//            }
        }
    }

    public TypeAnimateur getTypeAnimateur(java.lang.Integer id) {
        return getFacade().findById(id).orElse(null);
    }

    public List<TypeAnimateur> getItemsAvailableSelectMany() {
        return IteratorUtils.toList(getFacade().findAll().iterator());
    }

    public List<TypeAnimateur> getItemsAvailableSelectOne() {
        return IteratorUtils.toList(getFacade().findAll().iterator());
    }

    @FacesConverter(forClass = TypeAnimateur.class)
    public static class TypeAnimateurControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            TypeAnimateurController controller = (TypeAnimateurController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "typeAnimateurController");
            return controller.getTypeAnimateur(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof TypeAnimateur) {
                TypeAnimateur o = (TypeAnimateur) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), TypeAnimateur.class.getName()});
                return null;
            }
        }

    }

}
