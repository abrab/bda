package org.onee.dfc.ifpm.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.IteratorUtils;
import org.onee.dfc.ifpm.config.Messages;
import org.onee.dfc.ifpm.model.PrioriteTheme;
import org.onee.dfc.ifpm.repository.PrioriteThemeRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.annotation.SessionScope;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import org.onee.dfc.ifpm.controller.util.JsfUtil.PersistAction;
import org.onee.dfc.ifpm.controller.util.JsfUtil;

@Controller("prioriteThemeController")
@SessionScope
@Slf4j
@RequiredArgsConstructor
public class PrioriteThemeController implements Serializable {

    private final PrioriteThemeRepository ejbFacade;
    private final Messages messages;

    private List<PrioriteTheme> items = null;
    private PrioriteTheme selected;

    public PrioriteTheme getSelected() {
        return selected;
    }

    public void setSelected(PrioriteTheme selected) {
        this.selected = selected;
    }

    private PrioriteThemeRepository getFacade() {
        return ejbFacade;
    }

    public PrioriteTheme prepareCreate() {
        selected = new PrioriteTheme();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, messages.get("PrioriteThemeCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, messages.get("PrioriteThemeUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, messages.get("PrioriteThemeDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<PrioriteTheme> getItems() {
        if (items == null) {
            items = IteratorUtils.toList(getFacade().findAll().iterator());
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().save(selected);
                } else {
                    getFacade().delete(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (Exception ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, messages.get("PersistenceErrorOccured"));
                }
            }
//            catch (Exception ex) {
//                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
//                JsfUtil.addErrorMessage(ex, messages.get("PersistenceErrorOccured"));
//            }
        }
    }

    public PrioriteTheme getPrioriteTheme(java.lang.Integer id) {
        return getFacade().findById(id).orElse(null);
    }

    public List<PrioriteTheme> getItemsAvailableSelectMany() {
        return IteratorUtils.toList(getFacade().findAll().iterator());
    }

    public List<PrioriteTheme> getItemsAvailableSelectOne() {
        return IteratorUtils.toList(getFacade().findAll().iterator());
    }

    @FacesConverter(forClass = PrioriteTheme.class)
    public static class PrioriteThemeControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            PrioriteThemeController controller = (PrioriteThemeController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "prioriteThemeController");
            return controller.getPrioriteTheme(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof PrioriteTheme) {
                PrioriteTheme o = (PrioriteTheme) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), PrioriteTheme.class.getName()});
                return null;
            }
        }

    }

}
