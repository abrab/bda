package org.onee.dfc.ifpm.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.IteratorUtils;
import org.onee.dfc.ifpm.config.Messages;
import org.onee.dfc.ifpm.model.Animateur;

import org.onee.dfc.ifpm.repository.AnimateurRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.annotation.SessionScope;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import org.onee.dfc.ifpm.controller.util.JsfUtil.PersistAction;
import org.onee.dfc.ifpm.controller.util.JsfUtil;


@Controller("animateurController")
@SessionScope
@Slf4j
@RequiredArgsConstructor
public class AnimateurController implements Serializable {

    private final AnimateurRepository ejbFacade;
    private final Messages messages;

    private List<Animateur> items = null;
    private Animateur selected;

    public Animateur getSelected() {
        return selected;
    }

    public void setSelected(Animateur selected) {
        this.selected = selected;
    }

    private AnimateurRepository getFacade() {
        return ejbFacade;
    }

    public Animateur prepareCreate() {
        selected = new Animateur();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, messages.get("AnimateurCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, messages.get("AnimateurUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, messages.get("AnimateurDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Animateur> getItems() {
        if (items == null) {
            items = IteratorUtils.toList(getFacade().findAll().iterator());
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().save(selected);
                } else {
                    getFacade().delete(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (Exception ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, messages.get("PersistenceErrorOccured"));
                }
            }
//            catch (Exception ex) {
//                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
//                JsfUtil.addErrorMessage(ex, messages.get("PersistenceErrorOccured"));
//            }
        }
    }

    public Animateur getAnimateur(java.lang.Integer id) {
        return getFacade().findById(id).orElse(null);
    }

    public List<Animateur> getItemsAvailableSelectMany() {
        return IteratorUtils.toList(getFacade().findAll().iterator());
    }

    public List<Animateur> getItemsAvailableSelectOne() {
        return IteratorUtils.toList(getFacade().findAll().iterator());
    }

    @FacesConverter(forClass = Animateur.class)
    public static class AnimateurControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            AnimateurController controller = (AnimateurController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "animateurController");
            return controller.getAnimateur(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Animateur) {
                Animateur o = (Animateur) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Animateur.class.getName()});
                return null;
            }
        }

    }

}
