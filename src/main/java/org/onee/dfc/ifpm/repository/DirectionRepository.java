package org.onee.dfc.ifpm.repository;

import org.onee.dfc.ifpm.model.Direction;
import org.springframework.data.repository.CrudRepository;

public interface DirectionRepository extends CrudRepository<Direction, Integer> {
}
