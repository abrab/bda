package org.onee.dfc.ifpm.repository;

import org.onee.dfc.ifpm.model.Animateur;
import org.springframework.data.repository.CrudRepository;

public interface AnimateurRepository extends CrudRepository<Animateur, Integer> {
}
