package org.onee.dfc.ifpm.repository;

import org.onee.dfc.ifpm.model.PrioriteTheme;
import org.springframework.data.repository.CrudRepository;

public interface PrioriteThemeRepository extends CrudRepository<PrioriteTheme, Integer> {
}
