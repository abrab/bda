package org.onee.dfc.ifpm.repository;

import org.onee.dfc.ifpm.model.Type;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TypeRepository extends CrudRepository<Type, Integer> {
}
