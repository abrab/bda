package org.onee.dfc.ifpm.repository;

import org.onee.dfc.ifpm.model.Session;
import org.springframework.data.repository.CrudRepository;

public interface SessionRepository extends CrudRepository<Session, Integer> {
}
