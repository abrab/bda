package org.onee.dfc.ifpm.repository;

import org.onee.dfc.ifpm.model.TypeFormation;
import org.springframework.data.repository.CrudRepository;

public interface TypeFormationRepository extends CrudRepository<TypeFormation, Integer> {
}
