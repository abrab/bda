package org.onee.dfc.ifpm.repository;

import org.onee.dfc.ifpm.model.TypeAnimateur;
import org.springframework.data.repository.CrudRepository;

public interface TypeAnimateurRepository extends CrudRepository<TypeAnimateur, Integer> {
}
