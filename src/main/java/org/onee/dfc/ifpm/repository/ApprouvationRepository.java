package org.onee.dfc.ifpm.repository;

import org.onee.dfc.ifpm.model.Approuvation;
import org.springframework.data.repository.CrudRepository;

public interface ApprouvationRepository extends CrudRepository<Approuvation, Integer> {
}
