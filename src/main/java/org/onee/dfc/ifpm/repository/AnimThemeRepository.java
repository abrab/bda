package org.onee.dfc.ifpm.repository;

import org.onee.dfc.ifpm.model.AnimTheme;
import org.springframework.data.repository.CrudRepository;

public interface AnimThemeRepository extends CrudRepository<AnimTheme, Integer> {
}
