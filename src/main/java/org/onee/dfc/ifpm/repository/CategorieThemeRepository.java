package org.onee.dfc.ifpm.repository;

import org.onee.dfc.ifpm.model.CategorieTheme;
import org.springframework.data.repository.CrudRepository;

public interface CategorieThemeRepository extends CrudRepository<CategorieTheme, Integer> {
}
