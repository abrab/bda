package org.onee.dfc.ifpm.repository;

import org.onee.dfc.ifpm.model.Fonction;
import org.springframework.data.repository.CrudRepository;

public interface FonctionRepository extends CrudRepository<Fonction, Integer> {
}
