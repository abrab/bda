package org.onee.dfc.ifpm.repository;

import org.onee.dfc.ifpm.model.Proposition;
import org.springframework.data.repository.CrudRepository;

public interface PropositionRepository extends CrudRepository<Proposition, Integer> {
}
