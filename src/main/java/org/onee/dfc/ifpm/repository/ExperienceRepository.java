package org.onee.dfc.ifpm.repository;

import org.onee.dfc.ifpm.model.Experience;
import org.springframework.data.repository.CrudRepository;

public interface ExperienceRepository extends CrudRepository<Experience, Integer> {
}
