package org.onee.dfc.ifpm.repository;

import org.onee.dfc.ifpm.model.Evaluation;
import org.springframework.data.repository.CrudRepository;

public interface EvaluationRepository extends CrudRepository<Evaluation, Integer> {
}
