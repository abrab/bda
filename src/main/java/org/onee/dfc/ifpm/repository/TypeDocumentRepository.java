package org.onee.dfc.ifpm.repository;

import org.onee.dfc.ifpm.model.TypeDocument;
import org.springframework.data.repository.CrudRepository;

public interface TypeDocumentRepository extends CrudRepository<TypeDocument, Integer> {
}
