package org.onee.dfc.ifpm.repository;

import org.onee.dfc.ifpm.model.SousDomain;
import org.springframework.data.repository.CrudRepository;

public interface SousDomainRepository extends CrudRepository<SousDomain, Integer> {
}
