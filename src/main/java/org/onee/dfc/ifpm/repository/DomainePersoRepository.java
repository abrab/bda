package org.onee.dfc.ifpm.repository;

import org.onee.dfc.ifpm.model.DomainePerso;
import org.springframework.data.repository.CrudRepository;

public interface DomainePersoRepository extends CrudRepository<DomainePerso, Integer> {
}
