package org.onee.dfc.ifpm.repository;

import org.onee.dfc.ifpm.model.Theme;
import org.springframework.data.repository.CrudRepository;

public interface ThemeRepository extends CrudRepository<Theme, Integer> {
}
