package org.onee.dfc.ifpm.mapper;

import org.onee.dfc.ifpm.dto.TypeDto;
import org.onee.dfc.ifpm.model.Type;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TypeMapper {

    TypeDto mapTypeToTypeDto (Type type);
    Type mapTypeDtoToType (TypeDto typeDto);
    List<TypeDto> mapTypeListToTypeDtoList(Iterable<Type> typesIterable);
}
