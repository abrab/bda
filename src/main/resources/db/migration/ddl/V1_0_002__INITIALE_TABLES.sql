SET
FOREIGN_KEY_CHECKS=0;
SET
SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET
time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


CREATE TABLE animateur
(
    id                          int(11) NOT NULL,
    accompagnateur              varchar(255) DEFAULT NULL,
    decision_simulation         varchar(255) DEFAULT NULL,
    observation1                varchar(255) DEFAULT NULL,
    observation2                varchar(255) DEFAULT NULL,
    observation3                varchar(255) DEFAULT NULL,
    simulation                  varchar(255) DEFAULT NULL,
    active_retraite             varchar(255) DEFAULT NULL,
    age                         datetime     DEFAULT NULL,
    assistance_foad             varchar(255) DEFAULT NULL,
    cadre_integration           varchar(255) DEFAULT NULL,
    capacite_accompagnement     varchar(255) DEFAULT NULL,
    date_de_naissance           datetime     DEFAULT NULL,
    date_depart                 datetime     DEFAULT NULL,
    date_embauche               datetime     DEFAULT NULL,
    datefp                      datetime     DEFAULT NULL,
    date_inscription            datetime     DEFAULT NULL,
    date_invitationfp           datetime     DEFAULT NULL,
    date_simulation             datetime     DEFAULT NULL,
    email                       varchar(255) DEFAULT NULL,
    fiche_appreciaion_entretien varchar(255) DEFAULT NULL,
    formation_pedagogique       varchar(255) DEFAULT NULL,
    matricule                   varchar(255) DEFAULT NULL,
    nom_prenom                  varchar(255) DEFAULT NULL,
    photo                       varchar(255) DEFAULT NULL,
    responsable                 varchar(255) DEFAULT NULL,
    sex                         varchar(255) DEFAULT NULL,
    tele                        varchar(255) DEFAULT NULL,
    approuvation_id             int(11) DEFAULT NULL,
    direction_id                int(11) DEFAULT NULL,
    domaine_perso_id            int(11) DEFAULT NULL,
    experience_id               int(11) DEFAULT NULL,
    fonction_code               int(11) DEFAULT NULL,
    sous_domain_id              int(11) DEFAULT NULL,
    type_animateur_id           int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE anim_theme
(
    id              int(11) NOT NULL,
    sa              bit(1) NOT NULL,
    sc              bit(1) NOT NULL,
    animateur_id    int(11) DEFAULT NULL,
    session_id      int(11) DEFAULT NULL,
    theme_codetheme int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE approuvation
(
    id       int(11) NOT NULL,
    intitule varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE categorie_theme
(
    id       int(11) NOT NULL,
    intitule varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE direction
(
    id       int(11) NOT NULL,
    intitule varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE document
(
    id               int(11) NOT NULL,
    date             datetime DEFAULT NULL,
    animateur_id     int(11) DEFAULT NULL,
    type_document_id int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE domaine_perso
(
    id              int(11) NOT NULL,
    domaineperso    varchar(255) DEFAULT NULL,
    txtdomaineperso varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE evaluation
(
    id                int(11) NOT NULL,
    animation         decimal(19, 2) DEFAULT NULL,
    application_aquis decimal(19, 2) DEFAULT NULL,
    aspects_technique decimal(19, 2) DEFAULT NULL,
    evaluation_theme  decimal(19, 2) DEFAULT NULL,
    evaluation_total  decimal(19, 2) DEFAULT NULL,
    methodolgie       decimal(19, 2) DEFAULT NULL,
    session_id        int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE experience
(
    id       int(11) NOT NULL,
    intitule varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE fonction
(
    code     int(11) NOT NULL,
    intitule varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE priorite_theme
(
    id       int(11) NOT NULL,
    intitule varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE proposition
(
    id                              int(11) NOT NULL,
    autretheme                      int(11) DEFAULT NULL,
    cv                              varchar(255) DEFAULT NULL,
    dateproposition                 datetime     DEFAULT NULL,
    fmi                             varchar(255) DEFAULT NULL,
    nbranimateurexistantbda         int(11) DEFAULT NULL,
    nbranimateurproposesansdoublant int(11) DEFAULT NULL,
    nbranimateurvcdoublant          int(11) DEFAULT NULL,
    nbrthemeproposevcanim           int(11) DEFAULT NULL,
    totalthemeproposesansdoublant   int(11) DEFAULT NULL,
    direction_id                    int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `session`
(
    id         int(11) NOT NULL,
    datedebut  datetime DEFAULT NULL,
    datefin    datetime DEFAULT NULL,
    dureenjour int(11) DEFAULT NULL,
    nbrheure   double   DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE sous_domain
(
    id                  int(11) NOT NULL,
    sousdomaineperso    varchar(255) DEFAULT NULL,
    txtsousdomaineperso varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE theme
(
    codetheme     int(11) NOT NULL,
    dureetheme    varchar(255) DEFAULT NULL,
    intituletheme varchar(255) DEFAULT NULL,
    categorie_id  int(11) DEFAULT NULL,
    priorite_id   int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE theme_type_formations
(
    theme_codetheme    int(11) NOT NULL,
    type_formations_id int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `type`
(
    id      int(11) NOT NULL,
    libelle varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE type_animateur
(
    id       int(11) NOT NULL,
    intitule varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE type_document
(
    id       int(11) NOT NULL,
    intitule varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE type_formation
(
    id                int(11) NOT NULL,
    intituleformation varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


ALTER TABLE animateur
    ADD PRIMARY KEY (id),
  ADD KEY FK45ko89twh5fwoeqv9iaao0yfb (approuvation_id),
  ADD KEY FK1wx7p89kruxoohba7gw9skerh (direction_id),
  ADD KEY FK52shmn5lmjwg2n2looi2ce7w (domaine_perso_id),
  ADD KEY FK16c670t1bx370dodnm0k1v3jr (experience_id),
  ADD KEY FKc35d1b85rfk9045oqr1sf6vfj (fonction_code),
  ADD KEY FK7n46gxn5vqajj3kgntxctrqgv (sous_domain_id),
  ADD KEY FKsgu9cjb037ugurq0539nvj9h6 (type_animateur_id);

ALTER TABLE anim_theme
    ADD PRIMARY KEY (id),
  ADD KEY FK3tek4tex3ai27iodjb34wuix (animateur_id),
  ADD KEY FK6o9enlat4rtxjn6w3mbsvoyiw (session_id),
  ADD KEY FKnpb1o18wyyfem0qn6lfqlgpvo (theme_codetheme);

ALTER TABLE approuvation
    ADD PRIMARY KEY (id);

ALTER TABLE categorie_theme
    ADD PRIMARY KEY (id);

ALTER TABLE direction
    ADD PRIMARY KEY (id);

ALTER TABLE document
    ADD PRIMARY KEY (id),
  ADD KEY FKkb1p3p4y2stie485gx6imwoxr (animateur_id),
  ADD KEY FKe8bxll1qey8hrnj2i9t12s6n7 (type_document_id);

ALTER TABLE domaine_perso
    ADD PRIMARY KEY (id);

ALTER TABLE evaluation
    ADD PRIMARY KEY (id),
  ADD KEY FKjnb5emr8j0sap1nemi8jrluld (session_id);

ALTER TABLE experience
    ADD PRIMARY KEY (id);

ALTER TABLE fonction
    ADD PRIMARY KEY (code);

ALTER TABLE priorite_theme
    ADD PRIMARY KEY (id);

ALTER TABLE proposition
    ADD PRIMARY KEY (id),
  ADD KEY FKqrjbnh3ch4y2gj9ksvhx64mfg (direction_id);

ALTER TABLE `session`
    ADD PRIMARY KEY (id);

ALTER TABLE sous_domain
    ADD PRIMARY KEY (id);

ALTER TABLE theme
    ADD PRIMARY KEY (codetheme),
  ADD KEY FK9qkvfusrfsccrdnvkgkn5cxk1 (categorie_id),
  ADD KEY FK7p58vahs5uincw8si1j1pjqir (priorite_id);

ALTER TABLE theme_type_formations
    ADD KEY FKn113ap596d97a1c2d9f2lvap9 (type_formations_id),
  ADD KEY FKjixx669i69ghk71xkfhr2ek5f (theme_codetheme);

ALTER TABLE `type`
    ADD PRIMARY KEY (id);

ALTER TABLE type_animateur
    ADD PRIMARY KEY (id);

ALTER TABLE type_document
    ADD PRIMARY KEY (id);

ALTER TABLE type_formation
    ADD PRIMARY KEY (id);


ALTER TABLE animateur
    MODIFY id int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE anim_theme
    MODIFY id int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE approuvation
    MODIFY id int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE categorie_theme
    MODIFY id int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE direction
    MODIFY id int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE document
    MODIFY id int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE domaine_perso
    MODIFY id int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE evaluation
    MODIFY id int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE experience
    MODIFY id int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE fonction
    MODIFY code int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE priorite_theme
    MODIFY id int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE proposition
    MODIFY id int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `session`
    MODIFY id int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE sous_domain
    MODIFY id int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE theme
    MODIFY codetheme int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `type`
    MODIFY id int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE type_animateur
    MODIFY id int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE type_document
    MODIFY id int (11) NOT NULL AUTO_INCREMENT;

ALTER TABLE type_formation
    MODIFY id int (11) NOT NULL AUTO_INCREMENT;


ALTER TABLE animateur
    ADD CONSTRAINT FK16c670t1bx370dodnm0k1v3jr FOREIGN KEY (experience_id) REFERENCES experience (id),
  ADD CONSTRAINT FK1wx7p89kruxoohba7gw9skerh FOREIGN KEY (direction_id) REFERENCES direction (id),
  ADD CONSTRAINT FK45ko89twh5fwoeqv9iaao0yfb FOREIGN KEY (approuvation_id) REFERENCES approuvation (id),
  ADD CONSTRAINT FK52shmn5lmjwg2n2looi2ce7w FOREIGN KEY (domaine_perso_id) REFERENCES domaine_perso (id),
  ADD CONSTRAINT FK7n46gxn5vqajj3kgntxctrqgv FOREIGN KEY (sous_domain_id) REFERENCES sous_domain (id),
  ADD CONSTRAINT FKc35d1b85rfk9045oqr1sf6vfj FOREIGN KEY (fonction_code) REFERENCES fonction (code),
  ADD CONSTRAINT FKsgu9cjb037ugurq0539nvj9h6 FOREIGN KEY (type_animateur_id) REFERENCES type_animateur (id);

ALTER TABLE anim_theme
    ADD CONSTRAINT FK3tek4tex3ai27iodjb34wuix FOREIGN KEY (animateur_id) REFERENCES animateur (id),
  ADD CONSTRAINT FK6o9enlat4rtxjn6w3mbsvoyiw FOREIGN KEY (session_id) REFERENCES session (id),
  ADD CONSTRAINT FKnpb1o18wyyfem0qn6lfqlgpvo FOREIGN KEY (theme_codetheme) REFERENCES theme (codetheme);

ALTER TABLE document
    ADD CONSTRAINT FKe8bxll1qey8hrnj2i9t12s6n7 FOREIGN KEY (type_document_id) REFERENCES type_document (id),
  ADD CONSTRAINT FKkb1p3p4y2stie485gx6imwoxr FOREIGN KEY (animateur_id) REFERENCES animateur (id);

ALTER TABLE evaluation
    ADD CONSTRAINT FKjnb5emr8j0sap1nemi8jrluld FOREIGN KEY (session_id) REFERENCES session (id);

ALTER TABLE proposition
    ADD CONSTRAINT FKqrjbnh3ch4y2gj9ksvhx64mfg FOREIGN KEY (direction_id) REFERENCES direction (id);

ALTER TABLE theme
    ADD CONSTRAINT FK7p58vahs5uincw8si1j1pjqir FOREIGN KEY (priorite_id) REFERENCES priorite_theme (id),
  ADD CONSTRAINT FK9qkvfusrfsccrdnvkgkn5cxk1 FOREIGN KEY (categorie_id) REFERENCES categorie_theme (id);

ALTER TABLE theme_type_formations
    ADD CONSTRAINT FKjixx669i69ghk71xkfhr2ek5f FOREIGN KEY (theme_codetheme) REFERENCES theme (codetheme),
  ADD CONSTRAINT FKn113ap596d97a1c2d9f2lvap9 FOREIGN KEY (type_formations_id) REFERENCES type_formation (id);
SET
FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
